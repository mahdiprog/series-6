package sbu.cs.multithread.pi;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PiThreadRunner {
    private static MathContext precision ;
    volatile private static BigDecimal pi = new BigDecimal(0);

    public static String piThreadRunner(int floatingPoint) throws InterruptedException{
        precision = new MathContext(floatingPoint + 3);
        CountDownLatch counter = new CountDownLatch(floatingPoint);
        ExecutorService executor = Executors.newFixedThreadPool(10);
        for(int i = 0 ; i <= floatingPoint; i++){
            final int final_i = i;
            executor.submit(()->{
                sum(formula(final_i));
                counter.countDown();
            });
        }
        executor.shutdown();
        counter.await();
        Thread.sleep(10); //TODO: find reason
        return pi.toString().substring(0 , floatingPoint + 2);
    }

    synchronized public static void sum(BigDecimal temp){
        pi = pi.add(temp,precision);
    }

    //formula reference from https://zaya.io/4ds6t
    synchronized public static BigDecimal formula(int n){
        final BigDecimal ONE = new BigDecimal(1);
        BigDecimal res = ONE.multiply(new BigDecimal(4)).divide(new BigDecimal(8*n + 1) , precision);
        res = res.add(ONE.multiply(new BigDecimal(-2)).divide(new BigDecimal(8*n + 4),precision));
        res = res.add(ONE.multiply(new BigDecimal(-1)).divide(new BigDecimal(8*n + 5), precision));
        res = res.add(ONE.multiply(new BigDecimal(-1)).divide(new BigDecimal(8*n + 6), precision));
        res = res.multiply(ONE.divide(new BigDecimal(16)).pow(n , precision));
        return res;
    }
}
