package sbu.cs.multithread.priority;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Runner {

    public static List<Message> messages = new ArrayList<>();
    public static CountDownLatch blackCountDownLatch ;
    public static CountDownLatch blueCountDownLatch ;
    public static CountDownLatch whiteCountDownLatch ;
    /**
     * add your codes to this function. this function is the caller function which will be called first.
     * changing other codes in this function is allowed.
     *
     * @param blackCount    number of black threads
     * @param blueCount     number of blue threads
     * @param whiteCount    number of white threads
     */
    public void run(int blackCount, int blueCount, int whiteCount) throws InterruptedException {
        List<ColorThread> colorThreads = new ArrayList<>();
        blackCountDownLatch = new CountDownLatch(blackCount);
        blueCountDownLatch = new CountDownLatch(blueCount);
        whiteCountDownLatch = new CountDownLatch(whiteCount);
        ExecutorService executor = Executors.newFixedThreadPool(10);

        //Black Threads
        for (int i = 0; i < blackCount; i++) {
            BlackThread blackThread = new BlackThread();
            colorThreads.add(blackThread);
            executor.submit(blackThread);
        }
        blackCountDownLatch.await();

        //Blue Threads
        for (int i = 0; i < blueCount; i++) {
            BlueThread blueThread = new BlueThread();
            colorThreads.add(blueThread);
            executor.submit(blueThread);
        }
        blueCountDownLatch.await();

        //White Threads
        for (int i = 0; i < whiteCount; i++) {
            WhiteThread whiteThread = new WhiteThread();
            colorThreads.add(whiteThread);
            executor.submit(whiteThread);
        }
        executor.shutdown();
        whiteCountDownLatch.await();
    }

    synchronized public static void addToList(Message message) {
        messages.add(message);
    }

    public List<Message> getMessages() {
        return messages;
    }

    /**
     *
     * @param args
     */
    public static void main(String[] args) {

    }
}
