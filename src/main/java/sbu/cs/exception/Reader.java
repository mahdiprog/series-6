package sbu.cs.exception;

import java.util.List;
import java.util.prefs.BackingStoreException;

public class Reader {

    /**
     * declare 2 Exception class. 1 for UnrecognizedCommand and 1 for NotImplementedCommand
     * iterate on function inputs and check for commands and throw exception when needed.
     *
     * @param args
     * @throws NotImplementedCommandException
     */
    public void readTwitterCommands(List<String> args) throws ApException{

        for(String arg : args){
            if(Util.getImplementedCommands().contains(arg)){
                continue;
            }
                
            else if(Util.getNotImplementedCommands().contains(arg)){
                throw new NotImplementedCommandException("Error! NotImplemented Commands");
            }
            else{
                throw new UnrecognizedCommandException("Error! Unrecognized Command");
            }
        }
    }

    /**
     * function inputs are String but odd positions must be integer parsable
     * a valid input is like -> "ap", "2", "beheshti", "3992", "20"
     * throw BadInput exception when the string is not parsable.
     *
     * @param args
     * @throws BadInput
     */
    public void read(String...args) throws BadInput{
        int count = 1;
        for(String arg : args){
            if(count % 2 == 0){
                if(!isInt(arg)){
                    throw new BadInput("Bad Input!");
                }
            }
            count++;
        }
    }

    public static boolean isInt(String str){
        for(int i = 0 ; i < str.length() ; i++){
            if(!Character.isDigit(str.charAt(i))){
                return false;
            }
        }
        return true;
    }
}
